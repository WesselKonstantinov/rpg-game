# RPG characters console application in Java
A console application that enables users to create an RPG 
character. Users can also choose items such as weapons and armor
that alter the power of their character.

## Installation
To run this application, you need to install JDK 17 along with
an IDE (e.g. IntelliJ).

## Usage
1) Clone this repo by running the command 
`git clone https://gitlab.com/WesselKonstantinov/rpg-game.git`
2) Open the project in IntelliJ and look for **Main** in the src folder
   (src/main/java/nl/experis/rpg/Main.java)
3) Run this program and you will be prompted for input

## Example
```
Welcome to this simple RPG demo!
Let's start with choosing a name for your character: Mysterious Mage
Now choose your character class (Mage, Ranger, Rogue or Warrior): Mage
Your character stats look as follows:
Name: Mysterious Mage
Level: 1
Strength: 1
Dexterity: 1
Intelligence: 8
Damage per second (DPS): 1,08

As a Mage, you can equip the following weapons in order to deal more damage:
- STAFF
- WAND
Which one would you like?
```