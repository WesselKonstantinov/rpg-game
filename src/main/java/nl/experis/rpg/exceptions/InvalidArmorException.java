package nl.experis.rpg.exceptions;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String message) {
        super(message);
    }
}
