package nl.experis.rpg;

import nl.experis.rpg.attributes.PrimaryAttributes;
import nl.experis.rpg.characters.Character;
import nl.experis.rpg.characters.Mage;
import nl.experis.rpg.characters.Ranger;
import nl.experis.rpg.characters.Rogue;
import nl.experis.rpg.characters.Warrior;
import nl.experis.rpg.exceptions.InvalidArmorException;
import nl.experis.rpg.exceptions.InvalidWeaponException;
import nl.experis.rpg.items.*;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/*
* This program is a basic console application that
* enables the user to create an RPG character. This involves
* instantiating the right character, equipping items for
* enhanced attributes and showing the correct character stats.
* */

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Welcome to this simple RPG demo!");

        Scanner inputReader = new Scanner(System.in);
        String playerCharacterName = "";
        String playerCharacterClass = "";

        // Keep prompting the user for input until they provide a name
        while (playerCharacterName.isEmpty()) {
            System.out.print("Let's start with choosing a name for your character: ");
            playerCharacterName = inputReader.nextLine();
        }

        // Keep prompting the user for input until they provide a valid character class name
        while (!playerCharacterClass.equals("mage") &&
                !playerCharacterClass.equals("ranger") &&
                !playerCharacterClass.equals("rogue") &&
                !playerCharacterClass.equals("warrior")) {
            System.out.print("Now choose your character class (Mage, Ranger, Rogue or Warrior): ");
            playerCharacterClass = inputReader.nextLine().toLowerCase().trim();
        }

        Character playerCharacter = switch (playerCharacterClass) {
            case "mage" -> new Mage(playerCharacterName, new PrimaryAttributes(1, 1, 8));
            case "ranger" -> new Ranger(playerCharacterName, new PrimaryAttributes(1, 7, 1));
            case "rogue" -> new Rogue(playerCharacterName, new PrimaryAttributes(2, 6, 1));
            case "warrior" -> new Warrior(playerCharacterName, new PrimaryAttributes(5, 2, 1));
            default -> null;
        };

        System.out.println("Your character stats look as follows:");
        TimeUnit.SECONDS.sleep(1); // Add a slight delay to control the rate at which information is output
        System.out.println(playerCharacter);

        TimeUnit.SECONDS.sleep(1);
        playerCharacterClass = playerCharacterClass.substring(0, 1).toUpperCase() + playerCharacterClass.substring(1);
        System.out.println("As a " + playerCharacterClass + ", you can equip the following weapons in order to deal more damage:");
        for (WeaponType compatibleWeaponType : playerCharacter.getCompatibleWeaponTypes()) {
            System.out.println("- " + compatibleWeaponType);
        }

        String playerWeaponType;
        Weapon playerWeapon;
        while (true) {
            System.out.print("Which one would you like? ");
            playerWeaponType = inputReader.nextLine().toLowerCase().trim();
            if (!playerWeaponType.equals("staff") &&
                    !playerWeaponType.equals("wand") &&
                    !playerWeaponType.equals("bow") &&
                    !playerWeaponType.equals("dagger") &&
                    !playerWeaponType.equals("axe") &&
                    !playerWeaponType.equals("hammer") &&
                    !playerWeaponType.equals("sword")) {
                continue;
            }

            playerWeapon = switch (playerWeaponType) {
                case "staff" -> new Weapon("Common Staff", 1, WeaponType.STAFF, 10, 0.9);
                case "wand" -> new Weapon("Common Wand", 5, WeaponType.WAND, 15, 1.2);
                case "bow" -> new Weapon("Common Bow", 1, WeaponType.BOW, 12, 0.8);
                case "dagger" -> new Weapon("Common Dagger", 1, WeaponType.DAGGER, 10, 1.1);
                case "axe" -> new Weapon("Common Axe", 1, WeaponType.AXE, 7, 1.1);
                case "hammer" -> new Weapon("Common Hammer", 3, WeaponType.HAMMER, 11, 1.3);
                case "sword" -> new Weapon("Common Sword", 5, WeaponType.SWORD, 15, 1.5);
                default -> null;
            };

            try {
                boolean successfullyEquipped = playerCharacter.equipWeapon(playerWeapon);
                if (successfullyEquipped) break;
            } catch (InvalidWeaponException e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println("\nEquipped weapon: " + playerWeapon.getType());
        System.out.println("Here are your updated stats:");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(playerCharacter);

        TimeUnit.SECONDS.sleep(1);
        System.out.println("As a " + playerCharacterClass + ", you also get to equip the following pieces of armor for enhanced attributes:");
        for (ArmorType compatibleArmorType: playerCharacter.getCompatibleArmorTypes()) {
            System.out.println("- " + compatibleArmorType);
        }

        String playerTargetSlot = "";
        while (!playerTargetSlot.equals("head") && !playerTargetSlot.equals("body") && !playerTargetSlot.equals("legs")) {
            System.out.print("First, choose your where you want to equip your armor (Head, Body, or Legs): ");
            playerTargetSlot = inputReader.nextLine().toLowerCase().trim();
        }

        Slot chosenSlot = switch(playerTargetSlot) {
            case "head" -> Slot.HEAD;
            case "body" -> Slot.BODY;
            case "legs" -> Slot.LEGS;
            default -> null;
        };

        String playerArmorType;
        Armor playerArmor;
        while (true) {
            System.out.print("Now choose your armor: ");
            playerArmorType = inputReader.nextLine().toLowerCase().trim();
            if (!playerArmorType.equals("cloth") &&
                !playerArmorType.equals("leather") &&
                !playerArmorType.equals("mail") &&
                !playerArmorType.equals("plate")) {
                continue;
            }

            playerArmor = switch (playerArmorType) {
                case "cloth" -> new Armor("Common Cloth", 1, ArmorType.CLOTH, new PrimaryAttributes(0, 0, 5));
                case "mail" -> new Armor("Common Mail", 1, ArmorType.MAIL, new PrimaryAttributes(2, 3, 0));
                case "leather" -> new Armor("Common Leather", 3, ArmorType.LEATHER, new PrimaryAttributes(0, 10, 0));
                case "plate" -> new Armor("Common Plate", 5, ArmorType.PLATE, new PrimaryAttributes(10, 0, 0));
                default -> null;
            };

            try {
                boolean successfullyEquipped = playerCharacter.equipArmor(playerArmor, chosenSlot);
                if (successfullyEquipped) break;
            } catch (InvalidArmorException e) {
                System.out.println(e.getMessage());
            }
        }

        assert playerArmor != null; // Prevent NullPointerException warning
        System.out.println("\nEquipped armor: " + playerArmor.getType());
        System.out.println("Here are your updated stats:");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(playerCharacter);

        TimeUnit.SECONDS.sleep(1);
        System.out.println("You're all set! Now it's time to level up!");
        playerCharacter.levelUp();

        TimeUnit.SECONDS.sleep(2);
        System.out.println("Congratulations! You've levelled up!");
        System.out.println("Your new stats look as follows:");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(playerCharacter);

        System.out.println("Thanks for trying out this demo!");
    }
}
