package nl.experis.rpg.items;

import nl.experis.rpg.attributes.PrimaryAttributes;

public class Armor extends Item {
    private ArmorType type;
    private PrimaryAttributes attributes;

    public Armor(String name, int level, ArmorType type, PrimaryAttributes attributes) {
        super(name, level);
        this.type = type;
        this.attributes = attributes;
    }

    public ArmorType getType() {
        return type;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }
}
