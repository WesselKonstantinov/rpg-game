package nl.experis.rpg.items;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE,
}
