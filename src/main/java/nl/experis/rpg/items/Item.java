package nl.experis.rpg.items;

public abstract class Item {
    protected String name;
    protected int level;

    public Item(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
