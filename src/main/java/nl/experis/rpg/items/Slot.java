package nl.experis.rpg.items;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON,
}
