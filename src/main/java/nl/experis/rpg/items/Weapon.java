package nl.experis.rpg.items;

public class Weapon extends Item {
    private WeaponType type;
    private int damage;
    private double attackSpeed;

    public Weapon(String name, int level, WeaponType type, int damage, double attackSpeed) {
        super(name, level);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public WeaponType getType() {
        return type;
    }

    public double getWeaponDamagePerSecond() {
        return damage * attackSpeed;
    }
}
