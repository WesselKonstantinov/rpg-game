package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import nl.experis.rpg.items.ArmorType;
import nl.experis.rpg.items.WeaponType;

public class Ranger extends Character {

    public Ranger(String name, PrimaryAttributes basePrimaryAttributes) {
        super(name, basePrimaryAttributes);
        this.strengthGain = 1;
        this.dexterityGain = 5;
        this.intelligenceGain = 1;
        this.compatibleWeaponTypes = new WeaponType[]{WeaponType.BOW};
        this.compatibleArmorTypes = new ArmorType[]{ArmorType.LEATHER, ArmorType.MAIL};
    }

    @Override
    public int getTotalMainPrimaryAttribute() {
        return this.getTotalAttributes().get("dexterity");
    }
}
