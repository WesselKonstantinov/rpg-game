package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import nl.experis.rpg.items.ArmorType;
import nl.experis.rpg.items.WeaponType;

public class Mage extends Character {

    public Mage(String name, PrimaryAttributes basePrimaryAttributes) {
        super(name, basePrimaryAttributes);
        this.strengthGain = 1;
        this.dexterityGain = 1;
        this.intelligenceGain = 5;
        this.compatibleWeaponTypes = new WeaponType[]{WeaponType.STAFF, WeaponType.WAND};
        this.compatibleArmorTypes = new ArmorType[]{ArmorType.CLOTH};
    }

    @Override
    public int getTotalMainPrimaryAttribute() {
        return this.getTotalAttributes().get("intelligence");
    }
}
