package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import nl.experis.rpg.items.ArmorType;
import nl.experis.rpg.items.WeaponType;

public class Warrior extends Character {

    public Warrior(String name, PrimaryAttributes basePrimaryAttributes) {
        super(name, basePrimaryAttributes);
        this.strengthGain = 3;
        this.dexterityGain = 2;
        this.intelligenceGain = 1;
        this.compatibleWeaponTypes = new WeaponType[]{WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD};
        this.compatibleArmorTypes = new ArmorType[]{ArmorType.MAIL, ArmorType.PLATE};
    }

    @Override
    public int getTotalMainPrimaryAttribute() {
        return this.getTotalAttributes().get("strength");
    }
}
