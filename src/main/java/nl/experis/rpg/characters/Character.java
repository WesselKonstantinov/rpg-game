package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import nl.experis.rpg.exceptions.InvalidArmorException;
import nl.experis.rpg.exceptions.InvalidWeaponException;
import nl.experis.rpg.items.*;

import java.util.Arrays;
import java.util.HashMap;

public abstract class Character {
    protected String name;
    protected int level;
    protected PrimaryAttributes basePrimaryAttributes;
    protected int strengthGain;
    protected int dexterityGain;
    protected int intelligenceGain;
    protected WeaponType[] compatibleWeaponTypes;
    protected ArmorType[] compatibleArmorTypes;
    protected HashMap<Slot, Item> equipment;

    public Character(String name, PrimaryAttributes basePrimaryAttributes) {
        this.name = name;
        this.level = 1;
        this.basePrimaryAttributes = basePrimaryAttributes;
        this.equipment = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
    }

    public int getStrengthGain() {
        return strengthGain;
    }

    public int getDexterityGain() {
        return dexterityGain;
    }

    public int getIntelligenceGain() {
        return intelligenceGain;
    }

    public WeaponType[] getCompatibleWeaponTypes() {
        return compatibleWeaponTypes;
    }

    public ArmorType[] getCompatibleArmorTypes() {
        return compatibleArmorTypes;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public abstract int getTotalMainPrimaryAttribute();

    public double getCharacterDamagePerSecond() {
        Weapon equippedWeapon = (Weapon) equipment.get(Slot.WEAPON);
        double weaponDamagePerSecond = equippedWeapon != null ? equippedWeapon.getWeaponDamagePerSecond() : 1;
        return weaponDamagePerSecond * (1 + ((double)this.getTotalMainPrimaryAttribute()/100));
    }

    public void levelUp() {
        this.setLevel(getLevel() + 1);
        this.setBasePrimaryAttributes(new PrimaryAttributes(
                basePrimaryAttributes.getStrength() + this.getStrengthGain(),
                basePrimaryAttributes.getDexterity() + this.getDexterityGain(),
                basePrimaryAttributes.getIntelligence() + this.getIntelligenceGain()
        ));
    }

    public boolean canEquipWeapon(Weapon weapon) {
        // A weapon can only be equipped by a character when:
        // (1) it belongs to the list of compatible weapons
        // (2) the character has at least the required level for equipping the weapon
        return Arrays.asList(this.getCompatibleWeaponTypes()).contains(weapon.getType()) && this.getLevel() >= weapon.getLevel();
    }

    public boolean canEquipArmor(Armor armor) {
        // A piece of armor can only be equipped by a character when:
        // (1) it belongs to the list of compatible pieces of armor
        // (2) the character has at least the required level for equipping the piece of armor
        return Arrays.asList(this.getCompatibleArmorTypes()).contains(armor.getType()) && this.getLevel() >= armor.getLevel();
    }

    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (this.canEquipWeapon(weapon)) {
            this.getEquipment().put(Slot.WEAPON, weapon);
            return true;
        } else throw new InvalidWeaponException("Either you're equipping the wrong weapon or you need to level up");
    }

    public boolean equipArmor(Armor armor, Slot targetSlot) throws InvalidArmorException {
        if (this.canEquipArmor(armor)) {
            this.getEquipment().put(targetSlot, armor);
            return true;
        }  else throw new InvalidArmorException("Either you're equipping the wrong armor or you need to level up");
    }

    public HashMap<String, Integer> getTotalAttributes() {
        Armor[] storedArmors = {
                (Armor) this.getEquipment().get(Slot.BODY),
                (Armor) this.getEquipment().get(Slot.HEAD),
                (Armor) this.getEquipment().get(Slot.LEGS),
        };
        int armorStrength = 0;
        int armorDexterity = 0;
        int armorIntelligence = 0;

        for (Armor storedArmor : storedArmors) {
            armorStrength += storedArmor != null ? storedArmor.getAttributes().getStrength() : 0;
            armorDexterity += storedArmor != null ? storedArmor.getAttributes().getDexterity() : 0;
            armorIntelligence += storedArmor != null ? storedArmor.getAttributes().getIntelligence() : 0;
        }

        int totalStrength = this.getBasePrimaryAttributes().getStrength() + armorStrength;
        int totalDexterity = this.getBasePrimaryAttributes().getDexterity() + armorDexterity;
        int totalIntelligence = this.getBasePrimaryAttributes().getIntelligence() + armorIntelligence;

        HashMap<String, Integer> totalAttributes = new HashMap<>();
        totalAttributes.put("strength", totalStrength);
        totalAttributes.put("dexterity", totalDexterity);
        totalAttributes.put("intelligence", totalIntelligence);

        return totalAttributes;
    }

    public String toString() {
        return String.format("""
                Name: %s
                Level: %d
                Strength: %d
                Dexterity: %d
                Intelligence: %d
                Damage per second (DPS): %.2f
                """,
                this.getName(),
                this.getLevel(),
                this.getTotalAttributes().get("strength"),
                this.getTotalAttributes().get("dexterity"),
                this.getTotalAttributes().get("intelligence"),
                this.getCharacterDamagePerSecond());
    }
}
