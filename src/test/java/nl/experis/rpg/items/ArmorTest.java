package nl.experis.rpg.items;

import nl.experis.rpg.attributes.PrimaryAttributes;
import nl.experis.rpg.characters.Warrior;
import nl.experis.rpg.exceptions.InvalidArmorException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    void shouldNotBeEquippedWhenCharacterLevelTooLow() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Armor plateBodyArmor = new Armor("Common Plate Body Armor", 2, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));

        InvalidArmorException thrown = assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(plateBodyArmor, Slot.BODY));
        assertEquals("Either you're equipping the wrong armor or you need to level up", thrown.getMessage());
    }

    @Test
    void shouldNotBeEquippedWhenIncompatibleWithCharacter() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Armor cloth = new Armor("Common Cloth Head Armor", 1, ArmorType.CLOTH, new PrimaryAttributes(0, 0, 5));

        InvalidArmorException thrown = assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(cloth, Slot.HEAD));
        assertEquals("Either you're equipping the wrong armor or you need to level up", thrown.getMessage());
    }

    @Test
    void shouldBeEquippedWhenCompatibleWithCharacter() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Armor plateBodyArmor = new Armor("Common Plate Body Armor", 1, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));

        try {
            boolean successfullyEquipped = warrior.equipArmor(plateBodyArmor, Slot.BODY);
            assertTrue(successfullyEquipped);
            assertEquals(warrior.getEquipment().get(Slot.BODY), plateBodyArmor);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void shouldNotIncreaseTotalMainPrimaryAttributeWhenNotEquipped() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        int warriorTotalStrength = warrior.getTotalMainPrimaryAttribute();
        assertEquals(5, warriorTotalStrength);
    }

    @Test
    void shouldIncreaseTotalMainPrimaryAttributeWhenEquipped() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Armor plateBodyArmor = new Armor("Common Plate Body Armor", 1, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));

        try {
            warrior.equipArmor(plateBodyArmor, Slot.BODY);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        int warriorTotalStrength = warrior.getTotalMainPrimaryAttribute();
        assertEquals(6, warriorTotalStrength);
    }
}