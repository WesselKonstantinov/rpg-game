package nl.experis.rpg.items;

import nl.experis.rpg.attributes.PrimaryAttributes;
import nl.experis.rpg.characters.Warrior;
import nl.experis.rpg.exceptions.InvalidArmorException;
import nl.experis.rpg.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
     void shouldNotBeEquippedWhenCharacterLevelTooLow() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Weapon axe = new Weapon("Common Axe", 2, WeaponType.AXE, 7, 1.1);

        InvalidWeaponException thrown = assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(axe));
        assertEquals("Either you're equipping the wrong weapon or you need to level up", thrown.getMessage());
    }

    @Test
    void shouldNotBeEquippedWhenIncompatibleWithCharacter() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Weapon bow = new Weapon("Common Bow", 1, WeaponType.BOW, 12, 0.8);

        InvalidWeaponException thrown = assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(bow));
        assertEquals("Either you're equipping the wrong weapon or you need to level up", thrown.getMessage());
    }

    @Test
    void shouldBeEquippedWhenCompatibleWithCharacter() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Weapon axe = new Weapon("Common Axe", 1, WeaponType.AXE, 7, 1.1);

        try {
            boolean successfullyEquipped = warrior.equipWeapon(axe);
            assertTrue(successfullyEquipped);
            assertEquals(warrior.getEquipment().get(Slot.WEAPON), axe);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void shouldDefaultToBaseDPSWhenNoWeaponEquipped() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        double characterDamagePerSecond = warrior.getCharacterDamagePerSecond();
        assertEquals(1.05, characterDamagePerSecond);
    }

    @Test
    void shouldUseWeaponDPSWhenWeaponEquipped() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Weapon axe = new Weapon("Common Axe", 1, WeaponType.AXE, 7, 1.1);

        try {
            warrior.equipWeapon(axe);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }

        double characterDamagePerSecond = warrior.getCharacterDamagePerSecond();
        assertEquals(8.085, characterDamagePerSecond);
    }

    @Test
    void shouldUseWeaponDPSAndArmorBonusWhenWeaponAndArmorEquipped() {
        Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));
        Weapon axe = new Weapon("Common Axe", 1, WeaponType.AXE, 7, 1.1);
        Armor plateBodyArmor = new Armor("Common Plate Body Armor", 1, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));

        try {
            warrior.equipWeapon(axe);
            warrior.equipArmor(plateBodyArmor, Slot.BODY);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        double characterDamagePerSecond = warrior.getCharacterDamagePerSecond();
        assertEquals(8.162, characterDamagePerSecond);
    }
}