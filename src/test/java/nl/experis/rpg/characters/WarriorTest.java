package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    private final Warrior warrior = new Warrior("Wicked Warrior", new PrimaryAttributes(5, 2, 1));

    @Test
    void shouldBeLevelOneWhenCreated() {
        assertEquals(1, warrior.getLevel());
    }

    @Test
    void shouldHaveProperDefaultAttributesAtLevelOne() {
        assertEquals(5, warrior.getBasePrimaryAttributes().getStrength());
        assertEquals(2, warrior.getBasePrimaryAttributes().getDexterity());
        assertEquals(1, warrior.getBasePrimaryAttributes().getIntelligence());
    }

    @Test
    void shouldGainLevelOnLevelUp() {
        warrior.levelUp();
        assertEquals(2, warrior.getLevel());
    }

    @Test
    void shouldHaveAttributesIncreasedOnLevelUp() {
        warrior.levelUp();
        assertEquals(8, warrior.getBasePrimaryAttributes().getStrength());
        assertEquals(4, warrior.getBasePrimaryAttributes().getDexterity());
        assertEquals(2, warrior.getBasePrimaryAttributes().getIntelligence());
    }
}