package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    private final Ranger ranger = new Ranger("Robust Ranger", new PrimaryAttributes(1, 7, 1));

    @Test
    void shouldBeLevelOneWhenCreated() {
        assertEquals(1, ranger.getLevel());
    }

    @Test
    void shouldHaveProperDefaultAttributesAtLevelOne() {
        assertEquals(1, ranger.getBasePrimaryAttributes().getStrength());
        assertEquals(7, ranger.getBasePrimaryAttributes().getDexterity());
        assertEquals(1, ranger.getBasePrimaryAttributes().getIntelligence());
    }

    @Test
    void shouldGainLevelOnLevelUp() {
        ranger.levelUp();
        assertEquals(2, ranger.getLevel());
    }

    @Test
    void shouldHaveAttributesIncreasedOnLevelUp() {
        ranger.levelUp();
        assertEquals(2, ranger.getBasePrimaryAttributes().getStrength());
        assertEquals(12, ranger.getBasePrimaryAttributes().getDexterity());
        assertEquals(2, ranger.getBasePrimaryAttributes().getIntelligence());
    }
}