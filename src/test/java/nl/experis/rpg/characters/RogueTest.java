package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    private final Rogue rogue = new Rogue("Raging Rogue", new PrimaryAttributes(2, 6, 1));

    @Test
    void shouldBeLevelOneWhenCreated() {
        assertEquals(1, rogue.getLevel());
    }

    @Test
    void shouldHaveProperDefaultAttributesAtLevelOne() {
        assertEquals(2, rogue.getBasePrimaryAttributes().getStrength());
        assertEquals(6, rogue.getBasePrimaryAttributes().getDexterity());
        assertEquals(1, rogue.getBasePrimaryAttributes().getIntelligence());
    }

    @Test
    void shouldGainLevelOnLevelUp() {
        rogue.levelUp();
        assertEquals(2, rogue.getLevel());
    }

    @Test
    void shouldHaveAttributesIncreasedOnLevelUp() {
        rogue.levelUp();
        assertEquals(3, rogue.getBasePrimaryAttributes().getStrength());
        assertEquals(10, rogue.getBasePrimaryAttributes().getDexterity());
        assertEquals(2, rogue.getBasePrimaryAttributes().getIntelligence());
    }

}