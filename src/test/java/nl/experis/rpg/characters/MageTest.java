package nl.experis.rpg.characters;

import nl.experis.rpg.attributes.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    private final Mage mage = new Mage("Mysterious Mage", new PrimaryAttributes(1, 1, 8));

    @Test
    void shouldBeLevelOneWhenCreated() {
        assertEquals(1, mage.getLevel());
    }

    @Test
    void shouldHaveProperDefaultAttributesAtLevelOne() {
        assertEquals(1, mage.getBasePrimaryAttributes().getStrength());
        assertEquals(1, mage.getBasePrimaryAttributes().getDexterity());
        assertEquals(8, mage.getBasePrimaryAttributes().getIntelligence());
    }

    @Test
    void shouldGainLevelOnLevelUp() {
        mage.levelUp();
        assertEquals(2, mage.getLevel());
    }

    @Test
    void shouldHaveAttributesIncreasedOnLevelUp() {
        mage.levelUp();
        assertEquals(2, mage.getBasePrimaryAttributes().getStrength());
        assertEquals(2, mage.getBasePrimaryAttributes().getDexterity());
        assertEquals(13, mage.getBasePrimaryAttributes().getIntelligence());
    }

}